<?php namespace CodeYellow\Illuminate\Config;

/**
 * An extended config file loader which allows loading files for a
 * particular "variant" or "flavor" of the application.  See
 * \CodeYellow\Illuminate\Config\Repository for more info.
 */
class FileLoader extends \Illuminate\Config\FileLoader
{
	/**
	 * Load the given configuration group.
	 *
	 * @param  string  $environment
	 * @param  string  $group
	 * @param  string  $namespace
	 * @return array
	 */
	public function load($environment, $group, $namespace = null)
	{
        // TODO: This shouldn't be duplicated but taken from Config (Repository)
        if (!isset($_SERVER['APP_VARIANT'])) {
            // We can't use Log class because it hasn't been
            // bootstrapped, but luckily there's always an
            // exception handler available at this point.
            throw new \Exception('No APP_VARIANT configured.  Please set this environment variable.');
        }
        $variant = $_SERVER['APP_VARIANT'];
		$items = array();

		// First we'll get the root configuration path for the environment which is
		// where all of the configuration files live for that namespace, as well
		// as any environment folders with their specific configuration items.
		$path = $this->getPath($namespace);

		if (is_null($path))
		{
			return $items;
		}

		// First we'll get the main configuration file for the groups. Once we have
		// that we can check for any environment specific files, which will get
		// merged on top of the main arrays to make the environments cascade.
		$file = "{$path}/{$group}.php";

		if ($this->files->exists($file))
		{
			$items = $this->files->getRequire($file);
		}

        // This is new: we check a variant-specific but
        // environment-generic file.
		$file = "{$path}/{$variant}/{$group}.php";

		if ($this->files->exists($file))
		{
			$items = $this->mergeEnvironment($items, $file);
		}

		// Finally we're ready to check for the environment specific configuration
		// file which will be merged on top of the main arrays so that they get
		// precedence over them if we are currently in an environments setup.
        // This has been tweaked to look under the variant.
		$file = "{$path}/{$variant}/{$environment}/{$group}.php";

		if ($this->files->exists($file))
		{
			$items = $this->mergeEnvironment($items, $file);
		}

		return $items;
	}
}