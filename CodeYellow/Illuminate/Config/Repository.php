<?php namespace CodeYellow\Illuminate\Config;

/**
 * An extended config repository which holds a "variant" or "flavor"
 * of the application.  This allows "forking" the application without
 * having to keep the different versions in separate branches (which
 * is a maintenance nightmare).
 *
 * Each installation can have its own "production" environment,
 * depending on the selected variant.  We don't use different
 * environments for the variants, because too many assumptions are
 * built in in various places.  For example, in the very core of
 * Laravel's start.php, there's an assumption that "testing" is the
 * name of the environment under which the tests are run.  However, we
 * can't have just one testing environment if we want to test the
 * variants separately.  And there are of course other places that
 * assume "production" is the only production env.
 */
class Repository extends \Illuminate\Config\Repository
{
    public function appVariant()
    {
        if (!isset($_SERVER['APP_VARIANT']))
            throw new \Exception('No APP_VARIANT configured.  Please set this environment variable.');
        else
            return $_SERVER['APP_VARIANT'];
    }
}