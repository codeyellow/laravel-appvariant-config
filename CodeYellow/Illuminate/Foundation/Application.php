<?php 
namespace CodeYellow\Illuminate\Foundation;

use CodeYellow\Illuminate\Config\FileLoader,
    Illuminate\Filesystem\Filesystem;

/**
 * Subclass for Laravel's Application class which allows overriding
 * the class used for loading configurations.  This can currently not
 * be done through any of the provided dependency injection
 * mechanisms.
 *
 * In your start.php, just instantiate this instead of the base class.
 */
class Application extends \Illuminate\Foundation\Application
{
    /**
     * Overwrite getConfigLoader to use our own FileLoader
     *
     * @return \Illuminate\Config\LoaderInterface
     */
    public function getConfigLoader()
    {
        return new FileLoader(new Filesystem, $this['path'].'/config');
    }

    /**
     * Evil hack required because Laravel insists on re-binding the
     * Config instance manually in its start.php bootstrapping code.
     *
     * Question: Should we override registerCoreContainerAliases(), or
     * does this hack suffice?  Simply overriding _only_ that doesn't
     * work because of the aforementioned explicit instantiation.
     */
	public function instance($abstract, $instance)
    {
        if ($abstract == 'config') {
            $l = $instance->getLoader();
            $e = $instance->getEnvironment();
            return parent::instance($abstract, new \CodeYellow\Illuminate\Config\Repository($l, $e));
        } else {
            return parent::instance($abstract, $instance);
        }
    }
}