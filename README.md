Laravel application variants
============================

This simple extension will add an additional level of configuration in
between the "root" configuration files and the environment of your
application.

This allows you to maintain various "flavours" of your application
(for instance, for various clients with different requirements and
custom business logic) without having to fork it and maintain several
branches, which would be a maintenance nightmare.

Installing
----------

Simply add `codeyellow/laravel-appvariant-config` to your
`composer.json`.  Then, when it has been installed, change the
instantiation of the standard Application class in
`bootstrap/start.php` from
```
#!php
$app = new \Illuminate\Foundation\Application;
```
to
```
#!php
$app = new CodeYellow\Illuminate\Foundation\Application;
```

After this is done, you must configure the app variant to use in your
web server's config.  For Apache that's

```
#!apacheconf
SetEnv APP_VARIANT myvar
```

In PHP-FPM, that's

```
#!ini
env[APP_VARIANT] = myvar
```

Using
-----

Now, when you would like to perform a particular task depending on the
current variant, you can request it from the `\Config` facade:

```
#!php
$variant = \Config::appVariant();
```

Pitfalls
--------

Unfortunately, there's no safe way to define a "default" variant (like
the default environment being "development"), which means that you
must now set the APP_VARIANT environment variable on every command
that you run.  This includes composer commands, because artisan hooks
into composer, and artisan always performs a complete bootstrap:

```
#!shell-session
$ composer install
*BOOM*
$ APP_VARIANT=myvar composer install
..okay...
```